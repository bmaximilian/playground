# zip()
-   Bundles the latest emissions
    of a number of observables
    into a single observable
-   indexes of bundled emissions
    must match
-   zipped observable will emit
    at the pace of the slowest
    ancestor