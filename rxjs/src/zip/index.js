/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Rx from 'rxjs';

const slowNumberObservable = Rx.Observable.interval(100);
const slowSquareObservable = Rx.Observable
    .interval(350)
    .map(n => n * n);

Rx.Observable
    .zip(
        slowNumberObservable,
        slowSquareObservable,
    )
    .subscribe(a => console.log(a));
