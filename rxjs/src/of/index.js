/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable.of(
    1,2,3,4,'Five', [], {}, [1,2,3]
).subscribe(a => console.log(a));