/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

export default () => {
    const stdIn = process.stdin;
    stdIn.setRawMode(true);
    stdIn.setEncoding('utf-8');

    const observable = Rx.Observable
        .fromEvent(stdIn, 'data');

    observable.subscribe(key => {
            if (key === '\u0003') {
                process.exit();
            }
        });

    return observable;
};
