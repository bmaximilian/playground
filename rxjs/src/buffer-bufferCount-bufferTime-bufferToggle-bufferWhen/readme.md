# buffer()

## buffer()
-   Collects values from source
    observable until provided
    observable emits
-   Provided observable can emit
    anything
-   Collected values are emitted
    as an array
-   Starts buffering again
    immediately
    
##  bufferCount()
-   Like buffer but waits until
    a specified number of values
    are emitted from source
    before emitting buffered
    values

## bufferTime()
-   Like buffer but waits a
    specified amount of time
    before emitting
    buffered values
    
## bufferToggle()
-   Like buffer but takes
    two arguments - an opening
    and a closing observable
    -   closing observable
        is provided as a
        factory function
-   bufferToggle starts a buffer
    when opening observable
    emits
-   Emits values when closing
    observable emits
-   Can have multiple buffers
    going simultaneously
    
## bufferWhen()
-   Like *bufferToggle()* but
    requires no opening
    observable
-   Like buffer but factory
    function is provided
    instead of observable