/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import Rx from 'rxjs';
import fromStdIn from '../fromStdIn';

const observer = e => console.log(e);

fromStdIn()
    .buffer(
        fromStdIn()
            .filter(key => key === 'p')
    )
    .subscribe(observer);
// Stores all keys and emits when key "p" is pressed
// "p" will be stored in a new buffer

fromStdIn()
    .bufferCount(10)
    .subscribe(observer);
// Stores exactly 10 keys and emits when the 11th key is pressed
// 11th key will be stored in a new buffer

fromStdIn()
    .bufferTime(1000)
    .subscribe(observer);
// Stores every key pressed in a second
// Emits all the stored data, even if no key is pressed

fromStdIn()
    .bufferToggle(
        fromStdIn().filter(k => k === 'p'),
        () => fromStdIn().filter(k => k === 'o')
    )
    .subscribe(observer);
// Stores all keys pressed after "p" is pressed
// Emits when "o" is pressed

fromStdIn()
    .bufferWhen(
        () => Rx.Observable.timer(1000)
    )
    .subscribe(observer);
// Works now like bufferTime()
// Stores every key pressed in a second
// Emits all the stored data, even if no key is pressed