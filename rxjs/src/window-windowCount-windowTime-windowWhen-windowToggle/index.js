/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';
import fromStdIn from '../fromStdIn';

Rx.Observable
    .interval(100)
    .window(
        Rx.Observable.interval(1000)
    )
    .concatMap(a => a.toArray())
    .subscribe(a => console.log(a));
// An array of values from interval(100) in timespan of
// one interval(1000) step

Rx.Observable
    .interval(100)
    .windowCount(10)
    .concatMap(a => a.toArray())
    .subscribe(a => console.log(a));
// An array of 10 elements
// (interval(100) fires 10 times in one window)

Rx.Observable
    .interval(100)
    .windowTime(1000)
    .concatMap(a => a.toArray())
    .subscribe(a => console.log(a));
// Every value that is emitted in 1 second

Rx.Observable
    .interval(100)
    .windowToggle(
        fromStdIn().filter(key => key === 'p'),
        () => fromStdIn().filter(key => key === 'o'),
    )
    .concatMap(a => a.toArray())
    .subscribe(a => console.log(a));
// Every value that is emitted between pressing "p" (as start) and "o" (as end)

Rx.Observable
    .interval(100)
    .windowWhen(
        () => Rx.Observable.timer(1000),
    )
    .concatMap(a => a.toArray())
    .subscribe(a => console.log(a));
// Every value that is emitted in 1 second
