# window() & windowCount() & windowTime() & windowWhen() & windowToggle()

## window()
-   All emissions within a
    specified window of time
    are bundled into an
    array and emitted together
-   Like buffer, but emissions are
    bundled into an array
-   A provided observable
    indicates then to "close"
    the window
    -   New window is opened
        immediately
    -   Observable emits an array
        at this time
    -   First window opens
        automatically


## windowCount()
-   Like window, but shifts to
    the next window when the
    current window has
    accumulated a specified number
    of values
    
## windowTime()
-   Like window, but shifts to
    the next window after the
    current window has been
    opened for a specified amount
    of time

## windowToggle
-   Like window, but also
    takes two observables - 
    one which opens a window
    and another which closes
    it
-   Closing observable is a
    factory function like in
    *bufferToggle()*

## windowWhen
-   Like window but opening
    observable is a factory 
    function