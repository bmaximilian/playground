# throttle() & throttleTime()

## throttle()
-   Does not emit any observables
    until a duration of time,
    specified by the provided
    observable, has passed
    between source emissions
-   only emits the latest value

## throttleTime()
-   Like throttle, except
    duration is determined
    by a specified number
    and not an observable
