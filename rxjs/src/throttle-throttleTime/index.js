/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import fromStdIn from '../fromStdIn';

fromStdIn()
    .throttleTime(1000)
    .subscribe(a => console.log(a));
// Throttles the input 1 second and emits the last value

fromStdIn()
    .throttle(() =>fromStdIn().filter(key => key === 'p'))
    .subscribe(a => console.log(a));
// Every time when "p" is pressed, the next value will be emitted