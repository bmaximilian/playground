# reduce() & scan()

## reduce(function, initialValue)
-   Equivalent to
    *Array.prototype.reduce()*
-   Aggregate all the elements
    of an observable after it
    completes
    
## scan(function, initialValue)
-   Every time the source
    observable emits, aggregate
    all the values so far and
    emit the aggregated value
    -   Like *reduce()* but
        emits multiple times