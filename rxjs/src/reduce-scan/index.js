/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(500)
    .take(10)
    .reduce((accumulator, value) => accumulator + value, 0)
    .subscribe(e => console.log(`reduce: ${e}`));

Rx.Observable
    .interval(500)
    .take(10)
    .scan((accumulator, value) => accumulator + value, 0)
    .subscribe(e => console.log(`scan: ${e}`));