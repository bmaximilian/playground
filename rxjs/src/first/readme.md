# first()
-   roughly equivalent to
    *Array.prototype.find()*
-   Creates an observable which
    completes as soon as the source
    observable emits an acceptable
    value
-   Useful for extracting a value
    from an observable that will
    not complete, or that will
    take a long time to complete
    