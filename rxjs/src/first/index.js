/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(100)
    .first(n => n === 3)
    .subscribe(a => console.log(a));
