# forkJoin()
-   Runs a number of observables,
    waits until they all finish,
    then bundle the results
    and emit
    -   Forking - the process of
        running all the observables
        at once
    -   Joining - the process of
        combining the results
-   If any error, *forkJoin()*
    will error
-   Useful when you need
    the results of all of a
    number of different API calls
    or none at all
-   Resolves a very common web
    development use case