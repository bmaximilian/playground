/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Rx from 'rxjs';

const authAPI = Rx.Observable
    .timer(4000)
    .mapTo({
        name: 'Max',
        token: 'jverf34th34jhtv3j4nfb'
    });

const preferencesAPI = Rx.Observable
    .timer(1000)
    .mapTo({
        theme: 'dark',
    });

Rx.Observable
    .forkJoin(
        authAPI,
        preferencesAPI,
    )
    .subscribe(e => console.log(e));
// Works

const brokenButRequiredAPI = Rx.Observable
    .timer(3000)
    .mapTo(Rx.Observable.throw())
    .concatAll();

Rx.Observable
    .forkJoin(
        authAPI,
        preferencesAPI,
        brokenButRequiredAPI,
    )
    .subscribe(e => console.log(e));
// throws an error