/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .of(42)
    .do(() => console.log('What is the meaning of life?'))
    .delay(2000)
    .subscribe(n => console.log(n));

Rx.Observable
    .range(0,9)
    .delayWhen(n => Rx.Observable.timer(n * 1000))
    .subscribe(n => console.log(n));
