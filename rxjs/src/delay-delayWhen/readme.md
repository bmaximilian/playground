# delay() & delayWhen()

## delay(duration)
-   Emits values from the source
    array only after a specified
    duration has passed
-   duration is specified as
    a number
    
## delayWhen
-   like delay
-   instead of a number, a method
    which returns an observable
    is provided
-   delayWhen observable completes
    when provided observable is
    completed