# throw()
-   Creates an observable
    which immediately enters
    an error state while
    emitting no values
-   useful for testing
    error handling