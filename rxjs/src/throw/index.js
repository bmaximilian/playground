/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .throw()
    .subscribe({
        next: n => console.log(`${n} was emitted`),
        error: e => console.log('Entered error state', e),
    });