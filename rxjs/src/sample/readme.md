# sample()

-   Not equivalent to Lodash
    *sample()*
-   Emits the latest element
    from the source observable
    at a specified interval
-   Useful if the frequency
    at which new elements are
    added and the frequency as
    which you need to access
    elements vary greatly