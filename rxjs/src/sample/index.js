/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';
import fromStdIn from '../fromStdIn';

fromStdIn()
    .sample(
        Rx.Observable.interval(1000)
    )
    .subscribe(a => console.log(a));
