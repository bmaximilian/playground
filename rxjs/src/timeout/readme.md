# timeout()
-   Creates an observable that
    throws an error if the source
    observable waits longer
    than the specified duration
    to emit two consecutive values
-   once the source completes,
    timeout no longer applies