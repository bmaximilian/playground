/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import fromStdIn from '../fromStdIn';

fromStdIn()
    .take(10)
    .timeout(2000)
    .subscribe(a => console.log(`KEY: ${a}`));