/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(100)
    .map(n => ~~(n / 10))
    .distinctUntilChanged()
    .subscribe(n => console.log(n));