# distinctUntilChanged()

-   Creates an observable which
    only emits the latest value
    from the source observable
    if it is different than
    the one before it
-   Useful for an observable that
    tends to emit the same value
    many times in a row
