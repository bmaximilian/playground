# partition()
-   Separates a stream into two
    groups - one that passes
    the predicate function,
    and one that does not
-   Like combining the results
    of *filter()* with everything
    that was filtered out
    