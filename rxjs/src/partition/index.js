/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

const [even, odd] = Rx.Observable
    .range(1, 20)
    .partition(n => n % 2 === 0);

even.subscribe(e => console.log(`even: ${e}`));
odd.subscribe(e => console.log(`odd: ${e}`));