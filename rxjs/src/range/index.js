/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

const rangeObservable = Rx.Observable.range(1,9);

rangeObservable
    .subscribe(e => console.log(`Range: ${e}`));