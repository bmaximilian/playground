/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(1000)
    .let(source => {
        console.log(source);
        return Rx.Observable.interval(100)
    })
    .subscribe(e => console.log(e));
