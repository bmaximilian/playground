# let()

-   an operator which returns an
    observable that replaces
    the current one
    -   Receives the current
        observable as an argument
-   Usage promotes excessive
    trickiness