# race()
-   Waits until one observable
    from a group of provided
    observables emits,
    discards everything else
-   Subsequent emissions from
    the "winner" will be emitted
    while the "losers" of
    the race will be ignored