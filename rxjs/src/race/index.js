/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

const api1 = Rx.Observable
    .timer(3000, 100)
    .map(n => `api1 => ${n}`);

const api2 = Rx.Observable
    .timer(2000, 100)
    .map(n => `api2 => ${n}`);

Rx.Observable
    .race(api1, api2)
    .subscribe(a => console.log(a));
// emissions of api1 will be ignored because api2 was faster