/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .range(1, 20)
    .groupBy(n => n % 3)
    .mergeMap(nGroup => nGroup.toArray())
    .subscribe(a => console.log(a));