# groupBy()
-   After the source observable
    completes
    -   separate all the emitted
        values into groups
        based on an accessor
    -   Emit each of those groups
        as an observable