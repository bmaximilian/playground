/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Rx from 'rxjs';

/**
 * Publish
 */
const currencyTicker = Rx.Observable
    .interval(1000)
    .map(n => `Currency Info #${n} - [placeholder]`)
    .publish();

currencyTicker.subscribe(a => console.log('widget1', a));
currencyTicker.subscribe(a => console.log('widget2', a));
currencyTicker.connect();
// Connect is needed

/**
 * share
 */
const currencyTicker2 = Rx.Observable
    .interval(1000)
    .map(n => `Currency Info #${n} - [placeholder]`)
    .share();

currencyTicker2.subscribe(a => console.log('widget1', a));
currencyTicker2.subscribe(a => console.log('widget2', a));
// widget1 may get some values that widget2 doesn't
// if those values are emitted synchronously upon subscription

/**
 * share
 */
const shared = new Rx.BehaviorSubject(-1);
const currencyTicker3 = Rx.Observable
    .interval(1000)
    .map(n => `Currency Info #${n} - [placeholder]`)
    .multicast(shared);

currencyTicker3.subscribe(a => console.log('widget1', a));
currencyTicker3.subscribe(a => console.log('widget2', a));
currencyTicker3.connect();
// connect needed again => turned into BehaveiorSubject

