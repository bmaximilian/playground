# publish() & share() & multicast()

## publish()
-   Returns an observable with
    a special method, *connect()*
    -   Works similar to a Subject
-   Unlike normal observable,
    published observable does
    not start executing code
    as soon as it is subscribed
    to
    -   Multiple subscribers can
        subscribe and all get
        identical data
-   To start the functioning of
    the observable, like a normal
    observable responding
    to subscribe, call *connect()*
    
## share()
-   Like *publish()* but *connect()*
    is omitted
-   Observable starts executing
    code as soon as it is subscribed
    to, but does not start a new
    thread upon the 2nd subscription
    3rd subscription and so on
-   Useful for a long-lived process
    that gradually returns values
    -   i.e. a notifications service
        with many widgets subscribed
        to it

## multicast()
-   Like *publish*, but returns
    a Subject instead if an
    observable with a special
    property
-   BehaviorSubject, ReplaySubject
    and others can be used