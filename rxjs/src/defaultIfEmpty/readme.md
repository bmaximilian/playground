# defaultIfEmpty()
-   creates an observable that,
    if the source observable
    completes before emitting
    any values, emits the provided
    value
-   has no effect if the source
    observable emitted any values
    