/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .empty()
    .defaultIfEmpty(42)
    .subscribe(a => console.log('Where is the value???', a));