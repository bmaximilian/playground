# ignoreElements()
-   Doesn't emit any values from
    source observable, but does
    emit an error or complete
    state from the source
