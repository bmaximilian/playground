/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .range(1, 10)
    .ignoreElements()
    .subscribe({
       next: a => console.log(`Next: ${a}`),
       complete: a => console.log(`Complete: ${a}`),
    });
