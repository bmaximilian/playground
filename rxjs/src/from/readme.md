# From

-   converts an array, promise or
    iterator into an observable
    -   *fromPromise* can be used
        specially for promises
