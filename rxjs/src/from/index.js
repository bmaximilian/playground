/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

/**
 * With array
 */
const fibonacciArray = [
    1,1,2,3,5,8,13,21
];
const observer = a => console.log(a);

Rx.Observable
    .from(fibonacciArray)
    .subscribe(observer);

/**
 * With promise
 */
const fibonacciPromise = new Promise(r => r(fibonacciArray));
Rx.Observable
    .from(fibonacciPromise)
    .subscribe(observer);

/**
 * With generator
 */
function * fibonacciGenerator() {
    let a = 1;
    let b = 1;

    while (true) {
        let c = a + b;
        yield c;
        a = b;
        b = c;
    }
}
Rx.Observable
    .from(fibonacciGenerator())
    .take(20)
    .subscribe(observer);