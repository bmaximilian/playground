# single()
-   Emits just one value which
    passes a function after the
    source observable completes
-   If more than one value
    passes the predicate, an error
    will be thrown