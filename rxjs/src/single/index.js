/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .range(0, 1)
    .single()
    .subscribe(a => console.log(a));
// 0

Rx.Observable
    .range(0, 2)
    .single()
    .subscribe(a => console.log(a));
// Error

Rx.Observable
    .range(0, 2)
    .single(n => n % 2 !== 0)
    .subscribe(a => console.log(a));
// 1