# withLatestFrom
-   Creates new observable that
    combines emissions from
    the source observable
    with the latest value
    from a provided observable
-   Subscribers are notified only
    when the source observable
    emits,
    but get both values
    -   No notification is
        received when the
        provided observable
        emits