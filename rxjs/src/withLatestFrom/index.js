/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';
import fromStdIn from '../fromStdIn';

Rx.Observable
    .interval(1000)
    .withLatestFrom(fromStdIn())
    .subscribe(a => console.log(a));
