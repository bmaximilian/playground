# create(...)

-   creates a new observable which
    emits, completes and errors
    under custom circumstances
-   Powerful, but executing too
    much code inside create is
    an anti-pattern