/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Rx from 'rxjs';

const customObservable = Rx.Observable.create(observer => {
    observer.next(42);

    const i = setInterval(() => observer.next('foo'), 500);

    setTimeout(() => {
        clearInterval(i);
        observer.complete();
    }, 2500);
});

customObservable
    .subscribe({
        next: a => console.log('Next', a),
        complete: a => console.log('Complete', a),
    });
