/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(100)
    .take(5)
    .subscribe(e => console.log(`take ${e}`));

Rx.Observable
    .interval(100)
    .takeWhile(n => n < 7)
    .subscribe(e => console.log(`takeWhile ${e}`));

Rx.Observable
    .interval(100)
    .takeUntil(Rx.Observable.timer(2000))
    .subscribe(e => console.log(`takeUntil ${e}`));