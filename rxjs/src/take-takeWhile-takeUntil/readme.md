# take() takeWhile() takeUntil()

## take()
-   emits only the first
    few values of the source
    observable
-   number of emitted values is
    specified by provided number

## takeWhile()
-   like take, but emits values
    from the source only until a
    the provided function
    returns false
-   passing values subsequent
    to the first failing value
    will not be emitted

## takeUntil()
-   like take but emits values
    from the source observable
    only until provided
    observable emits