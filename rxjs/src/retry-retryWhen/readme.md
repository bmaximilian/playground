# retry() & retryWhen()

## retry()
-   If the source observable throws
    an error, suppress the error
    and try again a specified number
    of times
-   Number of repetitions is
    specified by a provided number
-   Has no effect if the source
    never errors

# retryWhen()
-   Like retry but retries the
    source when provided
    observable emits