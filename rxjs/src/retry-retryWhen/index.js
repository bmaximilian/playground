/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Rx from 'rxjs';

const shittyAPI = Rx.Observable
    .timer(1000)
    .do(() => console.log('You called the shitty API'))
    .do(a => { throw new Error(a) });

shittyAPI
    .retry(4)
    .subscribe(a => console.log(a));
// Tries 4 times

shittyAPI
    .retryWhen(() => Rx.Observable.interval(1000))
    .subscribe(a => console.log(a));
// Calls the shitty API every second