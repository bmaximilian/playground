# every()
-   Equivalent to
    *Array.prototype.every()*
-   Emits true if each element
    emitted by the source array
    passed a provided function
-   stops if provided function
    returns false
-   Only emits after the source
    completes
    