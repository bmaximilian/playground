/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .range(1,9)
    .map(n => n * 2)
    .do(n => console.log('Value', n))
    .every(n => n % 2 === 0)
    .subscribe(n => console.log(n));