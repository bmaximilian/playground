# pluck()
-   equivalent to lodash *pluck()*
-   used to map an observable
    of similar objects to a
    single property of those
    objects
-   A string, not a function
    is provided