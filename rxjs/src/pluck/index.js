/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rx';

const friendsAPI = Rx.Observable
    .interval(1000)
    .map(i => ({
      name: `Friend #${i}`,
      mutualFriends: i * 2 + 1,
    }))
    .pluck(name)
    .subscribe(a => console.log(a));
