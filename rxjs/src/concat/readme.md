# concat()
-   Loosely equivalent to
    *Array.prototype.concat()*
-   Creates an observable which
    emits all values from a
    source observable,
    then emits all values from
    a provided observable