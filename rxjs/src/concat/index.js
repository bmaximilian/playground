/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(100)
    .take(10)
    .concat(
        Rx.Observable
            .interval(100)
            .map(n => `#2: ${n}`)
            .skip(10)
            .take(20)
    )
    .subscribe(a => console.log(a));
