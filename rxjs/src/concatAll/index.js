/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';
import fromStdIn from '../fromStdIn';

Rx.Observable
    .of(
        Rx.Observable.range(1, 4),
        Rx.Observable.interval(100).take(6),
        fromStdIn().takeUntil(Rx.Observable.timer(7000)),
    )
    .concatAll()
    .subscribe(a => console.log(a));