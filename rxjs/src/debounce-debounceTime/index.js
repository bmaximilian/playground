/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs'
import fromStdIn from '../fromStdIn';

fromStdIn()
    .debounce(
        () => Rx.Observable.timer(1000)
    )
    .subscribe(e => console.log(e));

fromStdIn()
    .debounceTime(1000)
    .subscribe(e => console.log(e));
