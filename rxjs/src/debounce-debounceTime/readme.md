# debounce() & debounceTime()

## debounce()
-   Discard any values that are
    emitted whithin a specified
    period of time after the
    previous emission
-   Like throttle but with an
    initial value
-   Duration is specified by
    a provided observable
    
## debounceTime()
-   Like debounce but duration
    is specified by a number