/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .fromEvent(
        document.getElementsByTagName('form')[0],
        'click'
    )
    .subscribe(e => e.preventDefault());

Rx.Observable
    .fromEvent(
        document.getElementById('animal'),
        'change'
    )
    .map(e => e.target.value)
    .subscribe(e => console.log(e));


Rx.Observable
    .fromEvent(
        document.getElementById('text'),
        'change'
    )
    .map(e => e.target.value)
    .subscribe(e => console.log(e));
