# fromEvent()
-   Creates an observable which
    emits values as they
    come in from a generic
    event source
-   Event source can be many
    common JavaScript
    form controls
    -   Button
    -   Text input
    -   Other DOM Events