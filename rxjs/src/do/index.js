/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

const interval = Rx.Observable.interval(400);

interval
    .map(n => n * n)
    .do(n => console.log('N after map#1: ', n))
    .map(n => n / 3)
    .do(n => console.log('N after map#2: ', n))
    .map(n => n * n * n)
    .do(n => console.log('N after map#3: ', n))
    .map(n => n % 42)
    .subscribe(a => console.log(a));