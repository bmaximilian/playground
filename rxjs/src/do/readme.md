# do()
-   does a thing
-   executes a side effect such
    as a *console.log()*
-   more complex side effects
    (API calls etc) should not
    be handled with *do()*
-   Receives the last emitted
    value as an argument, but
    doesn't return anything
    (last emitted value is passed
    to new operator automatically)
-   Can't change the emitted value