# last()
-   Returns the last element of
    a source observable to pass
    a predicate, after that
    observable completes
-   unlike *first()* the source
    must complete