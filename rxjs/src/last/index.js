/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(100)
    .take(6)
    .last()
    .subscribe(a => console.log(a));
// 5

Rx.Observable
    .interval(100)
    .take(6)
    .last(n => n % 2 === 0)
    .subscribe(a => console.log(a));
// 4