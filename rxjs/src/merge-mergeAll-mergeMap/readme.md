# merge() mergeAll() mergeMap()

## merge()
-   creates a new observable
    which combines the source
    and provided observable
-   works like concat but all
    observables are subscribed
    to at once
    -   does not wait for
        previous observable
        to complete to start
        the next one
-   hard to determine post-merge
    what the source was
    
## mergeAll()
-   Merges all provided
    observables

## mergeMap()
-   if the source observable
    emits observables
    continuously subscribes
    to those and emit any
    value that comes from
    any of them