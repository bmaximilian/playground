/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Rx from 'rxjs';

const feed1 = Rx.Observable
    .interval(1000)
    .map(n => `Feed 1 Article No. ${n}`);

const feed2 = Rx.Observable
    .interval(650)
    .map(n => `Feed 2 Article No. ${n}`);

feed1
    .merge(feed2)
    .subscribe(e => console.log(e));

/**
 * Merge all
 */
const feed3 = Rx.Observable
    .interval(300)
    .map(n => `Feed 3 Article No. ${n}`);

Rx.Observable
    .of(
        feed1,
        feed2,
        feed3,
    )
    .mergeAll()
    .subscribe(e => console.log(e));

/**
 * mergeMap
 */
const config = {
    blue: feed1,
    red: feed2,
    green: feed3,
};
const colors = ['blue', 'red', 'green'];

Rx.Observable
    .from(colors)
    .mergeMap(color => config[color])
    .subscribe(e => console.log(e));
