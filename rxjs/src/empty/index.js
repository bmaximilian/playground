/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .empty()
    .subscribe({
        next: a => console.log('Emit: ', a),
        complete(a) {
            console.log('Complete', a);
        },
    });