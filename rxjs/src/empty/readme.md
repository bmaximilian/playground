# empty()
-   a static operator
-   creates an observable which
    completes immediately and
    returns no values
-   useful for testing,
    edge cases
