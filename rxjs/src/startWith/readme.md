# startWith()
-   creates a new observable that
    emits a provided value,
    then emits values from the
    source observable
-   Useful for asynchronous
    observables that may not
    return a value for some time
   