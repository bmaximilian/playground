/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .timer(4000, 500)
    .map(n => n + 1)
    .startWith(0)
    .subscribe(a => console.log(a));