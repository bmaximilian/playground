/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

const observer = e => console.log(e);

const ticker = Rx.Observable.interval(1000);
ticker.subscribe(observer);


const stopwatch = Rx.Observable.timer(1000);
stopwatch.subscribe(observer);