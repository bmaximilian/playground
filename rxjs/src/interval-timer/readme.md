# Interval & timer

## intreval(duration)
-   Emits a value each time
    the specified duration passes
-   Emits the numbers (0,1,2,3,4)

## timer(duration, [interval])
-   emits once after the
    specified duration has passed
-   If a second argument is passed,
    it will them emit each time
    that interval passes    