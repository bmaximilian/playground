/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

const range = Rx.Observable.range(1,9);
const observer = a => console.log(a);

range
    .map(n => n * n)
    .subscribe(observer);

const notificationAPI = Rx.Observable.interval(1000);
notificationAPI
    .mapTo({ type: 'NOTIVICATION_INCOMING' })
    .subscribe(observer);