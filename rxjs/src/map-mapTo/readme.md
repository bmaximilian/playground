# map() and mapTo()

## map()
-   Equivalent to *Array.prototype.map*
-   Converts each element to
    sth new based on provided
    mutator
    
## mapTo()
-   Converts each emitted value
    into a new value, without 
    regard for the emitted value
    