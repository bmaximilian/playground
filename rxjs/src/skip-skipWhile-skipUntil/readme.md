# skip() skipWhile() skipUntil()

## skip()
-   Ignores the first few
    elements of a
    source observable
-   Number provided as argument
    determines how many
    elements are skipped
    
## skipWhile()
-   ignores elements from a
    source observable until
    a provided function returns
    false

## skipUntil()
-   ignores elements from a
    source observable until
    a provided observable
    emits a value
