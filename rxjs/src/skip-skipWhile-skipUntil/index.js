/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';
import fromStdIn from '../fromStdIn';

fromStdIn()
    .skip(10)
    .subscribe(k => console.log(`Log: ${k}`));

fromStdIn()
    .skipWhile(v => v !== 'g')
    .subscribe(k => console.log(`Log: ${k}`));

fromStdIn()
    .skipUntil(Rx.Observable.timer(2000))
    .subscribe(k => console.log(`Log: ${k}`));