/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(400)
    .filter(n => n % 2 === 0)
    .subscribe(a => console.log(a));
