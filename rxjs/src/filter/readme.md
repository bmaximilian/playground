# filter()

-   Equivalent to
    *Array.prototype.filter*
-   Creates an observable that
    only emits the latest value
    from the source observable
    if it passes a predicate
    function