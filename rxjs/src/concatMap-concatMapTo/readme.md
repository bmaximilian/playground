# concatMap() & concatMapTo()

## concatMap()
-   Like *.map()* but the value
    returned from the mutator
    must be an observable
-   The observable returned from
    the mutator is subscribed to
    -   results are passed to the
        next observer
    
## concatMapTo()
-   Like *concatMap()* but maps
    to a constant observable
    with no regard for the
    incoming values