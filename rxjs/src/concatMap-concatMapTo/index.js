/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
import Rx from 'rxjs';

Rx.Observable
    .interval(100)
    .take(30)
    .concatMap(n => Rx.Observable.range(0, n + 1))
    .subscribe(a => console.log(`concatMap ${a}`));


Rx.Observable
    .interval(100)
    .take(30)
    .concatMapTo(Rx.Observable.range(0, 9))
    .subscribe(a => console.log(`concatMapTo ${a}`));
