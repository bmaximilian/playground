/**
 * Created on 12.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
 
require('webpack');
const path = require('path');

module.exports = {
    entry: {
        fromEvent: './src/fromEvent/index.js',
        combineLatest: './src/combineLatest/index.js',
    },
    output: {
        path: path.resolve(__dirname),
        publicPath: '/',
        filename: './src/[name]/bundle.js',
    },
    resolve: {
        extensions: ['.js', '.json'],
    },
    module: {
        loaders: [
            {
                test: /\.js/,
                use: {
                    loader: 'babel-loader',
                },
            },
        ],
    },
};
