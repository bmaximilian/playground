# RxJS

## What is RxJS?
- Library for dealing with streams of data
- Incoming data is represented by an observable
- operators can be used on resulting streams of data

## When should you use RxJS
### Needed
- You have an external event source
- Large long living application that has to do many complex things
- New data is pushed very often and you have to deal with it

### Not needed
- You have all the data as soon as the page loads
- Rare calls to outside apis
- rarely changing data in the application

## Observers
- Function that reacts to data
- can define handlers for complete and error events

## Observables
- Special function that returns many values
- Returns values at its own pace
- Can return values asyncronously or synchronously
- Can finish by entering a complete or error state
- Starts to execute when observer subscribes to it
- Cant change the data it's observing

### Standard Example
```javascript

const listObserver = (e) => {
	console.log(e);
};

const listObservable = new Rx.Observable(observer => {
	const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
	numbers.forEach(num => observer.next(num));
});

listObservable.subscribe(listObserver);
```

### Event Observable
```javascript

const eventObserver = (e) => console.log(`${e.target.tagName}#${e.target.id} clicked`);

const eventObservable = new Rx.Observable.fromEvent(
	document.getElementById('button'),
	'click',
);

eventObservable.subscribe(eventObserver);

```

## Subjects
- Like observables but dont start automatically when subscribed to
- Can have multiple sources of data
- Can have multiple subscribers
- Someone else calls next()

```javascript
const demoSubject = new Rx.Subject();
demoSubject.subscribe(name => console.log(name));

demoSubject.next('Max');
// Max

demoSubject.subscribe(name => console.log(`${name} knows sth about subjects`));

demoSubject.next('Robert');
// Robert
// Robert knows sth about subjects
```

## Behaveior subjects
- All the features of a subject 
- Remembers the last that was pushed
- New subscriber gets automatically the most recent thing

```javascript
const behaveiorSubject = new Rx.BehaveiorSubject(42);
const observer = n => console.log(`The meaning of life is ${n}`);

behaveiorSubject.subscribe(observer);
// The meaning of life is 42

behaveiorSubject.next(84);
// The meaning of life is 84
```

## Replay subjects
- Work like behaveior subjects but rebroadcast several most recent events
- useful if the last X events are interesting

```javascript
const replaySubject = new Rx.ReplaySubject(3);
const observer = n => console.log(n);
replaySubject.subscribe(observer)

replaySubject.next(0);
// 0
replaySubject.next(1);
// 1
replaySubject.next(2);
// 2
replaySubject.next(3);
// 3

replaySubject.subscribe(x => console.log(`${x} is awesome`));
// 1 is awesome
// 2 is awesome
// 3 is awesome

replaySubject.next(4);
// 4
// 4 is awesome
```

## Operators

- Convenient tools for transforming data incoming from observables and for creating new observables
	- Instance operators for transforming or aggregating data (map, sum)
	- Static operators for making new observables (create, interval, fromArray)

### Instance Operators
- consume data from an observable stream but never modifies the observable

```javascript
const demoObservable = new Rx.Observable(observer => {
	[7, 13, 17, 19, 23].forEach(n => observer.next(n));
});

demoObservable.subscribe(e => console.log(e));
// 7
// 13
// 17
// 19
// 23

const scanned = demoObservable
	.startsWith(0)
	.scan((accumulated, value) => accumulated + value);

scanned.subscribe(e => console.log(e));
// 0
// 7
// 20
// 37
// 56
// 79
```

### Static Operators
- Usually a property of Rx.Observable
- Generally returns a new, specialized observable instance

```javascript
const timer = new Rx.Observable.interval(1000);
timer.subscribe(t => console.log(t));
// 0
// 1
// 2
// 3
// 4
// ...
```

## Schedulers
- Define an execution context for observables
- Common use case: take the outputs of an observable that are a mix of synchronous and asyncronous and make them all async to manage them easier
- available as properties of Rx.Scheduler
