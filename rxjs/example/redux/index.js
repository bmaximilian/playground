/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Redux from './Redux';

const initialState = {
    todos: [
        'Write some code!',
        'Play a video game',
    ],
};

const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                ...state,
                todos: [
                    ...state.todos,
                    action.payload,
                ],
            };
        default:
            return state;
    }
};

const store = Redux.createStore(mainReducer, initialState);
store.subscribe(a => console.log(a));
store.dispatch({
    type: 'ADD_TODO',
    payload: 'Learn RxJS',
});