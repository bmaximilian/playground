/**
 * Created on 13.02.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */
import Rx from 'rxjs';

const singleton = Symbol('Redux-singleton-symbol');

/**
 * @class Redux
 */
class Redux {
    state = {};

    eventSource = null;
    scanned = null;

    /**
     * Returns the singleton instance
     */
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new Redux();
        }

        return this[singleton];
    }

    createStore(reducer, initialState = {}) {
        this.eventSource = new Rx.BehaviorSubject(initialState);

        this.scanned = this.eventSource
            .scan(reducer)
            .distinctUntilChanged()
            .do(newState => { this.state = newState });

        return this;
    }

    dispatch(action = {}) {
        this.eventSource.next(action);
    }

    subscribe(listener) {
        this.scanned.subscribe(listener)
    }

    getState() {
        return this.state;
    }
}

export default Redux.instance;
